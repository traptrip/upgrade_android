package com.example.andrey_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showRate(view: View) {
        // Create an Intent to start the second activity
        val rateIntent = Intent(this, SecondActivity::class.java)

//        val countString = textview_response.text.toString()
//        val count = Integer.parseInt(countString)
//        rateIntent.putExtra(SecondActivity.TOTAL_COUNT, count)

        startActivity(rateIntent)
    }

}

